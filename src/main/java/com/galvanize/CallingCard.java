
package com.galvanize;

public class CallingCard {
    private int centsPerMinute;
    private int money;
    private int minutes;
    CallingCard(int centsPerMinute) {
        this.centsPerMinute = centsPerMinute;
    }

    public void addMoney(int dollars) {
        money = dollars * 100;
        minutes += money / centsPerMinute;
        money = 0;
    }

    public int getRemainingMinutes() {
        System.out.println("Remaining minutes on card: " + minutes);
        return minutes;
    }

    public void useMinutes(int minutesUsed) {
        if(minutesUsed > minutes) {
            minutes = 0;
        }else {
            minutes -= minutesUsed;
        }
    }
}
