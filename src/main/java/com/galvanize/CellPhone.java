
package com.galvanize;

import java.util.ArrayList;

public class CellPhone {
    private ArrayList<String> callHistory = new ArrayList<>();
    private CallingCard callingCard;
    private boolean activeCall;
    private String currentNumber;
    private int callLength = 0;


    public CellPhone(CallingCard card) {
        callingCard = card;
    }

    public void startCall(String phoneNumber) {
        activeCall = true;
        currentNumber = phoneNumber;
    }

    public void endCall() {
        setCallHistory();
        activeCall = false;
        currentNumber = "Please dial a number";
        callLength = 0;
    }


    public void setCallHistory() {
        String minute = callLength > 1 ? "minutes" : "minute";

        if(!activeCall) {
            callHistory.add(String.format("%s (cut off at %s %s)", currentNumber, callLength, minute));
        }else {
            callHistory.add(String.format("%s (%s %s)", currentNumber, callLength, minute));
        }
    }

    public ArrayList<String> getCallHistory() {
        System.out.println(callHistory);
        return callHistory;
    }

    public void tick() {
        callLength++;
        callingCard.useMinutes(1);

        if(callingCard.getRemainingMinutes() <= 0) {
            activeCall = false;
            endCall();
        }
    }


}
