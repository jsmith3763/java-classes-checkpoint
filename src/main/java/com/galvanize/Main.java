package com.galvanize;

public class Main {
    public static void main(String[] args) {

        CallingCard card = new CallingCard(10);
        card.addMoney(1);

        CellPhone phone = new CellPhone(card);
        phone.startCall("555-1111");
        phone.tick();
        phone.endCall();

        phone.startCall("555-2222");
        phone.tick();
        phone.tick();
        phone.endCall();

        phone.getCallHistory();
        card.getRemainingMinutes();


    }
}
